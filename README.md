# TodoApi Example

## Links

- [Tutorial: Create a web API with ASP.NET Core
](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-web-api?view=aspnetcore-3.0&tabs=visual-studio-code)
- [Get started with Swashbuckle and ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-3.0&tabs=visual-studio)

## Commands

### TodoApi Project

```sh
$ pwd
~/Developer/TodoApi/

# new TodoApi project
$ dotnet new webapi -o TodoApi
$ cd TodoAPI

$ dotnet add package Microsoft.EntityFrameworkCore.SqlServer --version 3.0.0-*
$ dotnet add package Microsoft.EntityFrameworkCore.InMemory --version 3.0.0-*

# generate TodoItemController
$ dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design --version 3.0.0-*
$ dotnet add package Microsoft.EntityFrameworkCore.Design --version 3.0.0-*
$ dotnet tool install --global dotnet-aspnet-codegenerator
$ dotnet aspnet-codegenerator controller -name TodoItemsController -async -api -m TodoItem -dc TodoContext  -outDir Controllers

# app project to solution
$ cd ..
$ dotnet sln
$ dotnet sln add TodoApi/TodoApi.csproj
```

### Swagger

```sh
$ pwd
~/Developer/TodoApi
$ cd TodoApi

$ dotnet add TodoApi.csproj package Swashbuckle.AspNetCore -v 5.0.0-rc2
```

## Run

```sh
$ pwd
~/Developer/TodoApi/TodoApi
$ cd ..

$ dotnet restore
$ dotnet run --project TodoApi/TodoApi.csproj
```

- [https://localhost:5001/api/todoitmes](https://localhost:5001/api/todoitmes)
- [https://localhost:5001/swagger](https://localhost:5001/swagger)
